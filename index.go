package main

import (
	_ "database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
)

// repository contains the details of a repository
type repositorySummary struct {
	ID   string `json:"id"`
	Data string `json:"data"`
}

type repositories struct {
	Repositories []repositorySummary
}

// indexHandler calls `queryRepos()` and marshals the result as JSON
func allBlobsHandler(w http.ResponseWriter, req *http.Request) {
	repos := repositories{}

	err := getAllBlobs(&repos)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	out, err := json.Marshal(repos)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	fmt.Fprintf(w, string(out))
}
func BlobByIdHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)

	repos := repositories{}

	err := getBlobById(&repos)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	for _, item := range repos.Repositories {
		if item.ID == params["id"] {
			out, err := json.Marshal(item)
			if err != nil {
				http.Error(w, err.Error(), 500)
				return
			}
			fmt.Fprint(w, string(out))
		}
		fmt.Print("Nothing")

	}

	//for _, item := range repos.Repositories {
	//	if item.ID == "1" {
	//		out, err := json.Marshal(item)
	//		if err != nil {
	//			http.Error(w, err.Error(), 500)
	//			return
	//		}
	//		fmt.Fprintf(w, string(out))
	//	}
	//	panic("NO id = 1")
	//}

	//for _, item := range res {
	//	if item.ID == params["id"] {
	//		err := json.NewEncoder(w).Encode(item)
	//		if err != nil {
	//			return
	//		}
	//		return
	//	}
	//}
	//json.NewEncoder(w).Encode(&repositories{})
	//out, err := json.Marshal(repos)
	//if err != nil {
	//	http.Error(w, err.Error(), 500)
	//	return
	//}

	//for _, item := range res {
	//	if item.ID == params["id"] {
	//		err := json.NewEncoder(w).Encode(item)
	//		if err != nil {
	//			return
	//		}
	//		return
	//	}
	//}
	//json.NewEncoder(w).Encode(&repositories{})
	//fmt.Fprint(w, out)
}

func getAllBlobs(repos *repositories) error {
	rows, err := db.Query(`
		SELECT
			id,
			data
		FROM blob_data
		`)
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		repo := repositorySummary{}
		err = rows.Scan(
			&repo.ID,
			&repo.Data,
		)
		if err != nil {
			return err
		}

		repos.Repositories = append(repos.Repositories, repo)
	}
	err = rows.Err()
	if err != nil {
		return err
	}
	return nil
}

func getBlobById(repos *repositories) error {
	rows, err := db.Query(`
		SELECT
			id,
		    data
		FROM blob_data
		
		`)
	if err != nil {
		return err
	}

	defer rows.Close()
	for rows.Next() {
		repo := repositorySummary{}
		err = rows.Scan(
			&repo.ID,
			&repo.Data,
		)

		if err != nil {
			return err
		}
		//if IDindex == repo.ID {
		//
		//}
		repos.Repositories = append(repos.Repositories, repo)
	}
	err = rows.Err()
	if err != nil {
		return err
	}
	return nil
}
